.. XiVO-doc-devices Documentation master file.

*************************************************
XiVO Solutions Documentation on Devices
*************************************************

.. figure:: logo_xivo.png
   :scale: 70%

XiVO solutions developed by Avencall_ is a suite of PBX applications based on several free existing components including Asterisk_
and our own developments. This powerful and scalable solution offers a set of features for corporate telephony and call centers to power their business.

You may also have a look at our `development blog <http://xivo-solutions-blog.gitlab.io/>`_ for technical news about the solution

.. _Asterisk: http://www.asterisk.org/
.. _Avencall: https://www.xivo.solutions/


.. toctree::
   :maxdepth: 3

   devices/devices


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
