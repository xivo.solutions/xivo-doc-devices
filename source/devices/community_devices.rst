.. _compatible-devices:

Community Supported Devices
===========================

The community supported devices are only supported by the community. In other words, maintenance,
bug, corrections and features are developed by members of the XiVO community. XiVO does not
officially endorse support for these devices.

Aastra
------

6700i, 6800i and 9000i series:

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
6730i    |n|         8          |y|
6731i    |y|         8          |y|
6735i    |y|         26         |y|
6737i    |y|         30         |y|
6739i    |y|         55         |y|  
6753i    |y|         6          |y|
6755i    |y|         26         |y| 
6757i    |y|         30         |y|
6863i    |y|         0          |y|
6865i    |y|         8          |y|
6867i    |y|         38         |y|
6869i    |y|         68         |y|
9143i    |y|         7          |y|
9480i    |n|         6          |y|
9480CT   |n|         6          |y|
======== =========== ========== ============

.. note::
  The provisioning server doesn't know if you use a M680 or a M685. Therefore provisioning is done
  as if you had a M685 (the one with the more keys).
  See the below correspondency table between key number in Webi (:menuselection:`Function key` tab of user) and expansion module key number.
  **Note** that this example is given for a 6865 which has 8 programmable keys. Correspondency will change for a 6867 which has 38 softkeys.


+-----------------+-------------------------------------------+---------------------------------------+
|                 | M680 Key Number                           | M685 Key Number                       |
+ Webi Key Number +---------------------+---------------------+-----------------+---------------------+
|                 | Module 1            | Module 2            | Module 1        | Module 2            |
+=================+=====================+=====================+=================+=====================+
| **9**           | 1st                 |                     | 1st             |                     |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **24**          | 16th (and last one) |                     | 16th            |                     |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **25**          |                     |                     | 17th            |                     |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **92**          |                     |                     | 84th (last one) |                     |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **93**          |                     | 1st                 |                 | 1st                 |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **108**         |                     | 16th (and last one) |                 | 16th                |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **109**         |                     |                     |                 | 17th                |
+-----------------+---------------------+---------------------+-----------------+---------------------+
| **176**         |                     |                     |                 | 84th (and last one) |
+-----------------+---------------------+---------------------+-----------------+---------------------+


DECT Infrastructure
^^^^^^^^^^^^^^^^^^^

+-------------------+--------+--------+
|                   | RFP35  | RFP36  |
+===================+========+========+
| Provisioning      | N      | N      |
+-------------------+--------+--------+
| H-A               | N      | N      |
+-------------------+--------+--------+
| Directory XIVO    | N      | N      |
+-------------------+--------+--------+
| Funckeys          | 0      | 0      |
+-------------------+--------+--------+


Alcatel-Lucent
--------------

IP Touch series:

====================== =========== ========== ============
Model                  Tested [1]_ Fkeys [2]_ XiVO HA [3]_
====================== =========== ========== ============
4008 Extended Edition  |y|         4          |n|
4018 Extended Edition  |y|         4          |n|
====================== =========== ========== ============

Note that you *must not* download the firmware for these phones unless you
agree to the fact it comes from a non-official source.

For the plugin to work fully, you need these additional packages::

   apt-get install p7zip python-pexpect telnet


Avaya
-----

1200 series IP Deskphones (previously known as Nortel IP Phones):

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
1220 IP  |y|         0          |n|
1230 IP  |n|         0          |n|
======== =========== ========== ============


Cisco
-----

Cisco Small Business SPA300 series:

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
SPA301      |n|         1          |n|
SPA303      |n|         3          |n|
=========== =========== ========== ============

.. note:: Function keys are shared with line keys for all SPA phones

Cisco Small Business SPA500 series:

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
SPA501G     |y|         8          |n|
SPA502G     |n|         1          |n|
SPA504G     |y|         4          |n|
SPA508G     |y|         8          |n|
SPA509G     |n|         12         |n|
SPA512G     |n|         1          |n|
SPA514G     |n|         4          |n|
SPA525G     |y|         5          |n|
SPA525G2    |n|         5          |n|
=========== =========== ========== ============

The SPA500 expansion module is supported.

Cisco Small Business IP Phones (previously known as Linksys IP Phones)

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
SPA901      |n|         1          |n|
SPA921      |n|         1          |n|
SPA922      |n|         1          |n|
SPA941      |n|         4          |n|
SPA942      |y|         4          |n|
SPA962      |y|         6          |n|
=========== =========== ========== ============

.. note:: You must install the firmware of each SPA9xx phones you are using since they reboot in
          loop when they can’t find their firmware.

The SPA932 expansion module is supported.

ATAs:

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
PAP2        |n|         0          |n|
SPA2102     |n|         0          |n|
SPA8800     |n|         0          |n|
SPA112      |n|         0          |n|
SPA122      |n|         0          |n|
SPA3102     |n|         0          |n|
SPA8000     |n|         0          |n|
=========== =========== ========== ============

For best results, activate :doc_provi_advanced:`dhcp integration <>` on your XiVO.

These devices can be used to connect faxes. For better success with faxes some parameters
must be changed. You can read the :doc_fax_config:`documentation on fax <using-analog-gateways>` section.

.. note::
   If you want to manually resynchronize the configuration from the ATA device
   you should use the following url::

     http://ATA_IP/admin/resync?http://XIVO_IP:8667/CONF_FILE

   where :

      * *ATA_IP*    is the IP address of the ATA,
      * *XIVO_IP*   is the IP address of your XiVO,
      * *CONF_FILE* is one of ``spa3102.cfg``, ``spa8000.cfg``

.. _cisco-spa8000-fw-download:

**Cisco SPA8000 Firmware Download Procedure**

To install Cisco SPA8000 firmware, you need to manually download
the firmware files from the Cisco website and save them in the
:file:`/var/lib/xivo-provd/plugins/xivo-cisco-spa8000-6.1.11/var/cache` directory.

This directory is created by XiVO when you install the plugin (i.e. xivo-cisco-spa8000-6.1.11).
If you create the directory manually, the installation will fail.

* Go to https://software.cisco.com/
* Go to "Software download"
* In the search bar, search for "SPA8000"
* Click on "Analog Telephone Adaptor (ATA) Firmware"
* Then select under "All Release" the "6" menu, and under the "6" menu, select release "**6.1.11**"
* Finally download the file "**SPA8000_6.1.11_FW.zip**"
* Copy this file into the :file:`/var/lib/xivo-provd/plugins/xivo-cisco-spa8000-6.1.11/var/cache` directory
* Lastly, in the XiVO web interface, edit the plugin xivo-cisco-spa8000 and you'll then be able to click on the "install" button for the firmware

.. _cisco-spa100-fw-download:

**Cisco SPA100 1.4.1 SR5 Firmware Download Procedure**

To install the Cisco SPA100 1.4.1 SR5 firmware, you need to download its files manually
from the Cisco website and save them in the
:file:`/var/lib/xivo-provd/plugins/xivo-cisco-spa100-1.4.1-SR5/var/cache` directory.

This directory is created by XiVO when you install the plugin (i.e. xivo-cisco-spa100-1.4.1-SR5).
If you create the directory manually, the installation will fail.

* Go to https://software.cisco.com/
* Go to "Software download"
* In the search bar, search for either "SPA112" or "SPA122"
* Click on "Analog Telephone Adaptor (ATA) Firmware"
* Click on the "Download" logo on the right to download the file from the release 1.4.1 SR5
* It downloads the file "**SPA112-SPA122_1.4.1SR5_FW.zip**"
* Copy this file into the :file:`/var/lib/xivo-provd/plugins/xivo-cisco-spa100-1.4.1-SR5/var/cache` directory
* Lastly, in the XiVO web interface, edit the plugin xivo-cisco-spa100-1.4.1-SR5 and you'll then be able to click on the "install" button for the firmware


Cisco 7900 Series:

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
7905G       |y|         0          |y|
7906G       |y|         0          |y|
7911G       |y|         0          |y|
7912G       |y|         0          |y|
7920        |y|         0          |u|
7921G       |y|         0          |u|
7940G       |y|         1          |y|
7941G       |y|         1          |y|
7941G-GE    |y|         1          |y|
7942G       |y|         1          |y|
7960G       |y|         5          |y|
7961G       |y|         5          |y|
7962G       |y|         5          |y|  
=========== =========== ========== ============

.. warning:: These phones can only be used in SCCP mode. They are limited to the :doc_sccp:`features supported in XIVO's SCCP implementation <features>`.

.. _cisco-provisioning:

To install firmware for xivo-cisco-sccp plugins, you need to manually download
the firmware files from the Cisco website and save them in the
:file:`/var/lib/xivo-provd/plugins/$plugin-name/var/cache` directory.

This directory is created by XiVO when you install the plugin (i.e. xivo-cisco-sccp-legacy).
If you create the directory manually, the installation will fail.

.. warning:: Access to Cisco firmware updates requires a Cisco account with sufficient privileges.
   The account requires paying for the service and remains under the responsibility of the client or partner.
   Avencall is not responsible for these firmwares and does not offer any updates.

For example, if you have installed the ``xivo-cisco-sccp-legacy`` plugin and you want to install the ``7940-7960-fw``, ``networklocale`` and ``userlocale_fr_FR`` package, you must:

* Go to http://www.cisco.com
* Click on "Log In" in the top right corner of the page, and then log in
* Click on the "Support" menu
* Click on the "Downloads" tab, then on "Voice & Unified Communications"
* Select "IP Telephony", then "Unified Communications Endpoints", then the model of your phone (in this example, the 7940G)
* Click on "Skinny Client Control Protocol (SCCP) software"
* Choose the same version as the one shown in the plugin
* Download the file with an extension ending in ".zip", which is usually the last file in the list
* In the XiVO web interface, you'll then be able to click on the "install" button for the firmware

The procedure is similar for the network locale and the user locale package, but:

* Instead of clicking on "Skinny Client Control Protocol (SCCP) software", click on "Unified Communications Manager Endpoints Locale Installer"
* Click on "Linux"
* Choose the same version of the one shown in the plugin
* For the network locale, download the file named "po-locale-combined-network.cop.sgn"
* For the user locale, download the file named "po-locale-$locale-name.cop.sgn, for example "po-locale-fr_FR.cop.sgn" for the "fr_FR" locale
* Both files must be placed in :file:`/var/lib/xivo-provd/plugins/$plugin-name/var/cache` directory. Then install them in the XiVO Web Interface.

.. note:: Currently user and network locale 11.5.1 should be used for plugins xivo-sccp-legacy and xivo-cisco-sccp-9.4


Digium
------

+--------------------------------------------+-------+-------+-------+
|                                            | D40   | D50   | D70   |
+============================================+=======+=======+=======+
| Provisioning                               | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| H-A                                        | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Directory XIVO                             | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| Funckeys                                   | 2     | 14    | 106   |
+--------------------------------------------+-------+-------+-------+
| **Supported programmable keys**                                    |
+--------------------------------------------+-------+-------+-------+
| User with supervision function             | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| Group                                      | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Queue                                      | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Conference Room with supervision function  | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| **General Functions**                                              |
+--------------------------------------------+-------+-------+-------+
| Online call recording                      | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| Phone status                               | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Sound recording                            | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Call recording                             | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Incoming call filtering                    | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Do not disturb                             | HK    | NYT   | HK    |
+--------------------------------------------+-------+-------+-------+
| Group interception                         | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Listen to online calls                     | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| Directory access                           | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| Filtering Boss - Secretary                 | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| **Transfers Functions**                                            |
+--------------------------------------------+-------+-------+-------+
| Blind transfer                             | HK    | NYT   | HK    |
+--------------------------------------------+-------+-------+-------+
| Indirect transfer                          | HK    | NYT   | HK    |
+--------------------------------------------+-------+-------+-------+
| **Forwards Functions**                                             |
+--------------------------------------------+-------+-------+-------+
| Disable all forwarding                     | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Enable/Disable forwarding on no answer     | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Enable/Disable forwarding on busy          | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Enable/Disable forwarding unconditional    | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| **Voicemail Functions**                                            |
+--------------------------------------------+-------+-------+-------+
| Enable voicemail with supervision function | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Reach the voicemail                        | HK    | NYT   | HK    |
+--------------------------------------------+-------+-------+-------+
| Delete messages from voicemail             | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| **Agent Functions**                                                |
+--------------------------------------------+-------+-------+-------+
| Connect/Disconnect a static agent          | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Connect a static agent                     | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| Disconnect a static agent                  | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+
| **Parking Functions**                                              |
+--------------------------------------------+-------+-------+-------+
| Parking                                    | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| Parking position                           | N     | NYT   | N     |
+--------------------------------------------+-------+-------+-------+
| **Paging Functions**                                               |
+--------------------------------------------+-------+-------+-------+
| Paging                                     | Y     | NYT   | Y     |
+--------------------------------------------+-------+-------+-------+

.. note:: Some function keys are shared with line keys

Particularities:

* For best results, activate :doc_provi_advanced:`dhcp integration <>` on your XiVO.
* Impossible to do directed pickup using a BLF function key.
* Only supports DTMF in RFC2833 mode.
* Does not work reliably with Cisco ESW520 PoE switch. When connected to such a switch, the D40 tends to reboot randomly, and the D70 does not boot at all.
* It's important to not edit the phone configuration via the phones' web interface when using these phones with XiVO.
* Paging doesn't work.



Fanvil
------

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
C62P        |y|         5          |y|
=========== =========== ========== ============


Gigaset
-------

Also known as Siemens.

=========== =========== ========== ============
Model       Tested [1]_ Fkeys [2]_ XiVO HA [3]_
=========== =========== ========== ============
C470 IP     |n|         0          |n|
C475 IP     |n|         0          |n|
C590 IP     |n|         0          |n|
C595 IP     |n|         0          |n|
C610 IP     |n|         0          |n|
C610A IP    |n|         0          |n|
S675 IP     |n|         0          |n|
S685 IP     |n|         0          |n|
N300 IP     |n|         0          |n|
N300A IP    |n|         0          |n|
N510 IP PRO |n|         0          |n|
=========== =========== ========== ============


Jitsi
-----

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
Jitsi    |y|         |u|        |n|
======== =========== ========== ============


Mitel
-----

The Mitel 6700 Series and 6800 Series. See the Aastra_ section.


Panasonic
---------

Panasonic KX-HTXXX series:

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
KX-HT113   |n|         |u|         |n|
KX-HT123   |n|         |u|         |n|
KX-HT133   |n|         |u|         |n|
KX-HT136   |n|         |u|         |n|
======== =========== ========== ============

.. note:: This phone is for testing for the moment


Patton
------

FXS Gateways
^^^^^^^^^^^^

Analog VoIP gateways:

+--------------------------------------------+--------+--------+--------+--------+--------+--------+--------+
|                                            | SN4112 | SN4114 | SN4116 | SN4118 | SN4316 | SN4324 | SN4332 |
+============================================+========+========+========+========+========+========+========+
| Provisioning                               | Y      | Y      | Y      | Y      | Y      | Y      | Y      |
+--------------------------------------------+--------+--------+--------+--------+--------+--------+--------+
| H-A                                        | Y      | Y      | Y      | Y      | Y      | Y      | Y      |
+--------------------------------------------+--------+--------+--------+--------+--------+--------+--------+

If you have a gateway on which you would like to configure the FXO ports, you'll need to
write the FXO ports configuration manually by creating a :doc_provi_advanced:`custom template
<creating-custom-templates>` for your gateway.

It's only possible to enter a provisioning code on the first FXS port of a gateway. For example, if
you have a gateway with 8 FXS ports, the first port can be configured by dialing a provisioning code
from it, but ports 2 to 7 can only be configured via the XiVO web interface. Also, if you dial the
:doc_provi_basic:`"reset to autoprov" extension <resetting-a-device>` from any port, the configuration of
all the ports will be reset, not just the port on which the extension was dialed. These limitations
might go away in the future.


BRI Gateway
^^^^^^^^^^^

BRI VoIP gateways:

+--------------------------------------------+--------+
|                                            | SN4120 |
+============================================+========+
| Provisioning                               | Y      |
+--------------------------------------------+--------+
| H-A                                        | Y      |
+--------------------------------------------+--------+

To use the gateway you must, after having provisionned the gateway, configure a SIP trunk on the XiVO.

In :menuselection:`Trunk management -> SIP Protocol`, add a new SIP trunk with:

* Tab :menuselection:`General`:

  * :guilabel:`Name`: smartnode4120
  * :guilabel:`Authentication username`: smartnode4120

* Tab :menuselection:`Register`:

  * :guilabel:`Register`: yes
  * :guilabel:`Name`: smartnode4120
  * :guilabel:`Authentication username`: smartnode4120
  * :guilabel:`Password`: password
  * :guilabel:`Remote server`: the patton gateway IP address

* Tab :menuselection:`Signalling`:

  * :guilabel:`Monitoring`: yes


General notes
^^^^^^^^^^^^^

These gateways are configured with a few regional parameters (France by default). These parameters
are easy to change by writing a :doc_provi_advanced:`custom template <creating-custom-templates>`.

Telnet access and web access are enabled by default. You should change the default password by
setting an administrator password via a XiVO "template device".

By downloading and installing the Patton firmwares, you agree to the `Patton Electronics Company
conditions <http://www.patton.com/legal/eula.asp>`_.

To provision a gateway that was previously configured manually, use the following commands
on your gateway (configure mode), replacing XIVO_IP by the IP address of your XiVO server::

   profile provisioning PF_PROVISIONING_CONFIG
     destination configuration
     location 1 http://XIVO_IP:8667/$(system.mac).cfg
     activation reload graceful
     exit

   provisioning execute PF_PROVISIONING_CONFIG


Plantronics
-----------

.. important:: Only available for Freya LTS and above

These headphones can work with our `plantronics device action service`_, which allows you to
answer, hangup or unhold calls with their associated button.

.. _plantronics device action service : https://documentation.xivo.solutions/en/2020.18/usersguide/webrtc/index.html#plantronics-devices-actions-windows-only

+--------------------------------------------+-----------+
|                                            | Tested    |
+============================================+===========+
| Plantronics Blackwire 7225                 | N         |
+--------------------------------------------+-----------+
| Plantronics Voyager 5200 UC                | N         |
+--------------------------------------------+-----------+
| Plantronics Voyager B6200 UC               | N         |
+--------------------------------------------+-----------+
| Plantronics Voyager 4210 UC                | N         |
+--------------------------------------------+-----------+
| Plantronics Voyager Focus UC BT600         | Y         |
+--------------------------------------------+-----------+


Polycom
-------

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
SPIP320  |n|         0          |n|
SPIP321  |n|         0          |n|
SPIP330  |n|         0          |n|
SPIP331  |y|         N          |n|
SPIP335  |y|         0          |y| 
SPIP430  |n|         0          |n|
SPIP450  |y|         2          |n| 
SPIP501  |y|         0          |n|
SPIP550  |y|         3          |y| 
SPIP560  |y|         3          |n| 
SPIP600  |n|         0          |n|
SPIP650  |y|         47         |n| 
SPIP601  |n|         0          |n|
SPIP670  |n|         47         |n|

======== =========== ========== ============

SoundStation IP:

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
SPIP4000 |n|         0          |n|         
SPIP5000 |y|         0          |n|
SPIP6000 |y|         0          |n|
SPIP7000 |y|         0          |n|                 
======== =========== ========== ============

Others:

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
VVX101   |y|         0          |y|
VVX201   |y|         0          |y| 
VVX300   |y|         6          |y|
VVX310   |y|         6          |y|
VVX400   |y|         12         |y|
VVX410   |y|         12         |y|
VVX500   |y|         12         |y|
VVX600   |y|         0          |n| 
VVX1500  |n|         0          |n|
======== =========== ========== ============


Particularities:

* The latest Polycom firmwares can take a lot of time to download and install due to their size
  (~650 MiB). For this reason, these files are explicitly excluded from the XiVO backups.

* For directed call pickup to work via the BLF function keys, you need to make sure that the option
  :guilabel:`Set caller-id in dialog-info+xml notify` is enabled on your XiVO. This option is located on
  the :menuselection:`Services --> IPBX --> General settings --> SIP Protocol` page, in the
  :guilabel:`Signaling` tab.

  Also, directed call pickup via a BLF function key will not work if the extension number of the
  supervised user is different from its caller ID number.

* Default password is **9486** (i.e. the word "xivo" on a telephone keypad).

* On the VVX101 and VVX201, to have the two line keys mapped to the same SIP line, create a
  :doc_provi_advanced:`custom template <creating-custom-templates>` with the following content::

     {% extends 'base.tpl' -%}

     {% block remote_phonebook -%}
     {% endblock -%}

     {% block model_specific_parameters -%}
     reg.1.lineKeys="2"
     {% endblock -%}

  This is especially useful on the VVX101 since it supports a maximum of 1 SIP line and does not
  support function keys.

.. note:: (XiVO HA cluster) BLF function key saved on the master node are not available.


Snom
----

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
300      |n|         6          |y|
320      |y|         12         |y|
360      |n|         |u|        |y|
370      |y|         12         |y| 
820      |y|         4          |y|
821      |y|         12         |y|
870      |y|         15         |y|
MP       |n|         |u|        |y|
PA1      |n|         0          |y|
======== =========== ========== ============

.. note:: For some models, function keys are shared with line keys

.. warning:: If you are using Snom phones with HA, you should not assign multiple lines to the same device.

There's a known issue with the provisioning of Snom phones in XiVO:

* After a factory reset of a phone, if no language and timezone are set for the "default config device" in :menuselection:`XiVO --> Configuration --> Provisioning --> Template device`, you will be forced to select a default language and timezone on the phone UI.


Technicolor
-----------

Previously known as Thomson:

======== =========== ========== ============
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_
======== =========== ========== ============
ST2022   |n|         |u|        |u|
ST2030   |y|         10         |y|
======== =========== ========== ============

.. note:: Function keys are shared with line keys


.. _yealink-community-devices:

Yealink
-------

======== =========== ========== ============ ================
Model    Tested [1]_ Fkeys [2]_ XiVO HA [3]_ Plugin
======== =========== ========== ============ ================
CP860    |n|         0          |u|          xivo-yealink-v72
T19P     |y|         0          |y|          xivo-yealink-v84         
T20P     |y|         2          |y|          xivo-yealink-v73 
T21P     |y|         2          |y|          xivo-yealink-v72
T22P     |y|         3          |y|          xivo-yealink-v73
T26P     |y|         13         |y|          xivo-yealink-v73
T28P     |y|         16         |y|          xivo-yealink-v73
T32G     |y|         3          |n|          xivo-yealink-v70
T38G     |y|         16         |n|          xivo-yealink-v70 
T23P     |n|         3          |u|          xivo-yealink-v80
T27P     |y|         21         |y|          xivo-yealink-v80
T29G     |n|         27         |u|          xivo-yealink-v80
T41P     |y|         15         |y|          xivo-yealink-v81
T42G     |y|         15         |y|          xivo-yealink-v81  
T46G     |y|         27         |y|          xivo-yealink-v81  
T48G     |y|         27         |y|          xivo-yealink-v81
T49G     |y|         29         |y|          xivo-yealink-v80
======== =========== ========== ============ ================

.. note:: Some function keys are shared with line keys.


.. warning:: Those phones are not produced by Yealink since Q1 2021.	


Zenitel
-------

========== =========== ========== ============
Model      Tested [1]_ Fkeys [2]_ XiVO HA [3]_
========== =========== ========== ============
IP station |y|         1          |n|
========== =========== ========== ============

.. [1] ``Tested`` means the device has been tested by the XiVO development team and that
       the developers have access to this device.
.. [2] ``Fkeys`` is the number of programmable function keys that you can configure from the
       XiVO web interface. It is not necessarily the same as the number of physical function
       keys the device has (for example, a 6757i has 12 physical keys but you can configure 30
       function keys because of the page system).
.. [3] ``XiVO HA`` means the device is confirmed to work with :doc_ha:`XiVO HA <>`.

.. |y| replace:: Yes
.. |n| replace:: No
.. |ny| replace:: Not Yet
.. |u| replace:: ---
